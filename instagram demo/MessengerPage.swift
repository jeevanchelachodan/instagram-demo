//
//  MessengerPage.swift
//  instagram demo
//
//  Created by RP-14 on 05/05/22.
//

import UIKit

class MessengerPage: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource
                     ,UICollectionViewDelegateFlowLayout,UITableViewDelegate,UITableViewDataSource {
   
    
    var sectionArray:[String] = ["Primary","General","Request"]
    
    var messengerNameArray:[String] = ["Devendu","Jithincpaul","Sree","Adarsh","Basil Njarukadan","Nasif","AashikSaji","Comrade","Ananya","Ajoy","I_abhijith","Eldhose","Navin","Kennicha","BasilBiju","Allen","Rohan","Rohith","Atul","Abhaaay","Eshwar","NaveenDixon","VarunThombra"]
    
    var messageTitleArray:[String] = ["9+ messages","2 messages","5 messages","poda","sent a video","Ha ha","2 messages","vaa","3 messages","sent a post","daa","Ne evda","5 messages","sent a post","2 messages","videocall ended","Missed VideoCall","sugam","kollam","nthaanu mone","kali kanda nee","hi hi","onnula mone"]
    
    var messengerImageArray:[String] = ["photo1", "photo2", "photo3", "photo4", "photo5", "photo6", "photo7", "photo8", "photo9", "photo10","photo11", "photo12", "photo13", "photo14", "photo15", "photo16", "photo17", "photo18", "photo19", "photo20", "photo22", "photo23","photo21"]
    
    
    var tempNameArray = [""]
    var tempImageArray = [""]
    var tempTitleArray = [""]
    
    @IBOutlet weak var searchAreaView: UIView!
    @IBOutlet weak var messageSearch: UITextField!
    @IBOutlet weak var buttonFilter: UIButton!
    @IBOutlet weak var messageTable: UITableView!
    @IBOutlet weak var messageSection: UICollectionView!
    
    

    
     //MARK: - Message sections
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return sectionArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MessageSectionCell", for: indexPath) as!MessageSectionCell
        cell.sectionName.text = sectionArray[indexPath.item]
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (messageSection.frame.size.width)/3
        let height =  (messageSection.frame.size.height)
        return CGSize(width: width, height: height)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.item == 0{
            messengerNameArray = ["Devendu","Jithincpaul","Sree","Adarsh","Basil Njarukadan","Nasif","AashikSaji","Comrade","Ananya","Ajoy","I_abhijith","Eldhose","Navin","Kennicha","BasilBiju","Allen","Rohan","Rohith","Atul","Abhaaay","Eshwar","NaveenDixon","VarunThombra"]
            messageTitleArray = ["9+ messages","2 messages","5 messages","poda","sent a video","Ha ha","2 messages","vaa","3 messages","sent a post","daa","Ne evda","5 messages","sent a post","2 messages","videocall ended","Missed VideoCall","sugam","kollam","nthaanu mone","kali kanda nee","hi hi","onnula mone"]
            messengerImageArray = ["photo1", "photo2", "photo3", "photo4", "photo5", "photo6", "photo7", "photo8", "photo9", "photo10","photo11", "photo12", "photo13", "photo14", "photo15", "photo16", "photo17", "photo18", "photo19", "photo20", "photo22", "photo23","photo21"]
            
        }else if indexPath.item == 1{
            messengerNameArray.sort { ( lhs:String, rhs:String ) -> Bool in return lhs > rhs
            }
            messageTitleArray.sort { ( lhs:String, rhs:String ) -> Bool in return lhs > rhs
            }
            messengerImageArray.sort { ( lhs:String,rhs:String ) -> Bool in return lhs > rhs}
        }
        else {
            messengerNameArray.sort { ( lhs:String, rhs:String ) -> Bool in return lhs < rhs }
            messageTitleArray.sort { ( lhs:String, rhs:String ) -> Bool in return lhs < rhs
            }
            messengerImageArray.sort { ( lhs:String,rhs:String ) -> Bool in return lhs < rhs}
            
            }
        
        
            
        
    }

    
    //MARK: - Messengers Table
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messengerNameArray.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MessengerTableCell", for: indexPath)as!MessengerTableCell
        cell.messengerName.text = messengerNameArray [indexPath.row]
        cell.messageTitle.text = messageTitleArray [indexPath.row]
        cell.messengersDpImage.image = UIImage(named: messengerImageArray [indexPath.row])
        return cell
        
        
    }
    
   
   
    override func viewDidLoad() {
        super.viewDidLoad()
        searchAreaView.layer.cornerRadius = 5
        
        
        
        

        // Do any additional setup after loading the view.
    }
    
    @IBAction func backButton(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
