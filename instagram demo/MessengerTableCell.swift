//
//  MessengerTableCell.swift
//  instagram demo
//
//  Created by RP-14 on 06/05/22.
//

import UIKit

class MessengerTableCell: UITableViewCell {
    @IBOutlet weak var messengersDpImage: UIImageView!
    
    @IBOutlet weak var messengerName: UILabel!
    @IBOutlet weak var messageTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
