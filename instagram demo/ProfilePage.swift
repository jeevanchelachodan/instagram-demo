//
//  ProfilePage.swift
//  instagram demo
//
//  Created by RP-14 on 13/05/22.
//

import UIKit

class ProfilePage: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    var highlightArray = ["photo1","photo1", "photo30", "photo12", "photo13", "photo16","photo5", "photo14", "photo6", "photo9", "photo7"]
    
    var postsArray = ["photo1", "photo2", "photo3", "photo4", "photo5", "photo6", "photo7", "photo8", "photo9", "photo10","photo11", "photo12", "photo13", "photo14", "photo15", "photo16", "photo17", "photo18", "photo19", "photo20", "photo22", "photo23", "photo24", "photo25","photo26", "photo27", "photo28", "photo29", "photo30","photo1", "photo30", "photo12", "photo13", "photo16","photo5", "photo14", "photo6", "photo9", "photo7","photo10","photo11", "photo12", "photo13", "photo14", "photo15", "photo16", "photo17", "photo18", "photo19", "photo20", "photo22", "photo23", "photo24", "photo25"]
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == highlightsCollections{
            return highlightArray.count
        }else{
            return postsArray.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == highlightsCollections{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HighlightsCollectionPage", for: indexPath)as! HighlightsCollectionPage
            cell.highlightImage.image = UIImage(named: highlightArray[indexPath.item])
            cell.highlightImage.layer.cornerRadius = 30
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PostsCollectionPage", for: indexPath)as!PostsCollectionPage
            cell.postsImage.image = UIImage(named: postsArray[indexPath.item])
            return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == highlightsCollections{
            let width = (collectionView.layer.frame.width  )/6
            let height = collectionView.layer.frame.height
            return CGSize(width: width, height: height)
        }else{
            let width = (collectionView.layer.frame.width - 5 )/3
            let height = (collectionView.layer.frame.height - 5)/4
            return CGSize(width: width, height: height)
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == highlightsCollections{
            let navigate = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "StoryOpenPage") as! StoryOpenPage
            navigate.tempStory = highlightArray[indexPath.item]
            navigate.tempStoryDp = str
            navigate.tempStoryName = str2
            present(navigate, animated: true)
        }else{
            let navigate = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "StoryOpenPage") as! StoryOpenPage
            navigate.tempStory = postsArray[indexPath.item]
            present(navigate, animated: true)
        }
    }
    
    
    var str = "photo1"
    var str2 = "LOLA"
    @IBOutlet weak var profileDpImage: UIImageView!
    
    
    @IBOutlet weak var bckBtn: UIButton!
    @IBOutlet weak var postCount: UILabel!
    @IBOutlet weak var highlightsCollections: UICollectionView!
    @IBOutlet weak var postCollections: UICollectionView!
    @IBOutlet weak var profileName: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        profileDpImage.layer.cornerRadius = 35
        profileDpImage.image = UIImage(named: "photo17")
        postCount.text = postsArray.count.description
        
        profileDpImage.image = UIImage(named: str)
        profileName.text = str2
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func backButton(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
