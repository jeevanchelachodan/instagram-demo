//
//  HighlightsCollectionPage.swift
//  instagram demo
//
//  Created by RP-14 on 13/05/22.
//

import UIKit

class HighlightsCollectionPage: UICollectionViewCell {
    
    @IBOutlet weak var highlightImage: UIImageView!
}
