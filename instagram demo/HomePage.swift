//
//  HomePage.swift
//  instagram demo
//
//  Created by RP-14 on 05/05/22.
//

import UIKit

class HomePage: UIViewController, UICollectionViewDelegate,UICollectionViewDataSource,UIScrollViewDelegate {
    // MARK: - Database
    var imageStories = ["photo1","photo1", "photo30", "photo12", "photo13", "photo16","photo5", "photo14", "photo6", "photo9", "photo7"]
    
    var homePagePosts = ["photo1", "photo2", "photo3", "photo4", "photo5", "photo6", "photo7", "photo8", "photo9", "photo10","photo11", "photo12", "photo13", "photo14", "photo15", "photo16", "photo17", "photo18", "photo19", "photo20", "photo22", "photo23", "photo24", "photo25","photo26", "photo27", "photo28", "photo29", "photo30","photo1", "photo30", "photo12", "photo13", "photo16","photo5", "photo14", "photo6", "photo9", "photo7","photo10","photo11", "photo12", "photo13", "photo14", "photo15", "photo16", "photo17", "photo18", "photo19", "photo20", "photo22", "photo23", "photo24", "photo25","photo1", "photo2", "photo3", "photo4", "photo5", "photo6", "photo7", "photo8", "photo9", "photo10","photo11", "photo12", "photo13", "photo14", "photo15", "photo16", "photo17", "photo18", "photo19", "photo20", "photo22", "photo23", "photo24", "photo25","photo26", "photo27", "photo28", "photo29", "photo30","photo1", "photo30", "photo12", "photo13", "photo16","photo5", "photo14", "photo6", "photo9", "photo7","photo10","photo11", "photo12", "photo13", "photo14", "photo15", "photo16", "photo17", "photo18", "photo19", "photo20", "photo22", "photo23", "photo24", "photo25","photo1", "photo2", "photo3", "photo4", "photo5", "photo6", "photo7", "photo8", "photo9", "photo10","photo11", "photo12", "photo13", "photo14", "photo15", "photo16", "photo17", "photo18", "photo19", "photo20", "photo22", "photo23", "photo24", "photo25","photo26", "photo27", "photo28", "photo29", "photo30","photo1", "photo30", "photo12", "photo13", "photo16","photo5", "photo14", "photo6", "photo9", "photo7","photo10","photo11", "photo12", "photo13", "photo14", "photo15", "photo16", "photo17", "photo18", "photo19", "photo20", "photo22", "photo23", "photo24", "photo25","photo1", "photo2", "photo3", "photo4", "photo5", "photo6", "photo7", "photo8", "photo9", "photo10","photo11", "photo12", "photo13", "photo14", "photo15", "photo16", "photo17", "photo18", "photo19", "photo20", "photo22", "photo23", "photo24", "photo25","photo26", "photo27", "photo28", "photo29", "photo30","photo1", "photo30", "photo12", "photo13", "photo16","photo5", "photo14", "photo6", "photo9", "photo7","photo10","photo11", "photo12", "photo13", "photo14", "photo15", "photo16", "photo17", "photo18", "photo19", "photo20", "photo22", "photo23", "photo24", "photo25","photo1", "photo2", "photo3", "photo4", "photo5", "photo6", "photo7", "photo8", "photo9", "photo10","photo11", "photo12", "photo13", "photo14", "photo15", "photo16", "photo17", "photo18", "photo19", "photo20", "photo22", "photo23", "photo24", "photo25","photo26", "photo27", "photo28", "photo29", "photo30","photo1", "photo30", "photo12", "photo13", "photo16","photo5", "photo14", "photo6", "photo9", "photo7","photo10","photo11", "photo12", "photo13", "photo14", "photo15", "photo16", "photo17", "photo18", "photo19", "photo20", "photo22", "photo23", "photo24", "photo25"]
     
    var followersDp = ["photo1", "photo2", "photo3", "photo4", "photo5", "photo6", "photo7", "photo8", "photo9", "photo10","photo11", "photo12", "photo13", "photo14", "photo15", "photo16", "photo17", "photo18", "photo19", "photo20", "photo22", "photo23", "photo24", "photo25","photo26", "photo27", "photo28", "photo29", "photo30","photo1", "photo30", "photo12", "photo13", "photo16","photo5", "photo14", "photo6", "photo9", "photo7","photo10","photo11", "photo12", "photo13", "photo14", "photo15", "photo16", "photo17", "photo18", "photo19", "photo20", "photo22", "photo23", "photo24", "photo25"]
    
    var followesName = ["photo1", "photo2", "photo3", "photo4", "photo5", "photo6", "photo7", "photo8", "photo9", "photo10","photo11", "photo12", "photo13", "photo14", "photo15", "photo16", "photo17", "photo18", "photo19", "photo20", "photo22", "photo23", "photo24", "photo25","photo26", "photo27", "photo28", "photo29", "photo30","photo1", "photo30", "photo12", "photo13", "photo16","photo5", "photo14", "photo6", "photo9", "photo7","photo10","photo11", "photo12", "photo13", "photo14", "photo15", "photo16", "photo17", "photo18", "photo19", "photo20", "photo22", "photo23", "photo24", "photo25"]
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collectionStories{
            return imageStories.count
        }else{
            return homePagePosts.count
        }
        
            
    }
    // MARK: - Transportation
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == collectionStories {
            let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: "HomePageStories", for: indexPath) as! HomePageStories
            cell.stories.image = UIImage(named: imageStories [indexPath.item])
            cell.storiesNameLabel.text = imageStories[indexPath.item]
            cell.stories.layer.cornerRadius = 35
            //cell.tempStories = UIImage(named: imageStories[indexPath.item])
        
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomePagePosts", for: indexPath)as! HomePagePosts
            cell.homePagePostedImages.image = UIImage(named: homePagePosts[indexPath.item])
            cell.homePageFollowersDp.image = UIImage(named: followersDp[indexPath.item])
            cell.homePageFollowersName.text = followesName[indexPath.item]
            cell.homePageFollowersDp.layer.cornerRadius = 33
            
            


            return cell

        }
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == collectionStories{
            let navigate = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "StoryOpenPage")as!StoryOpenPage
            navigate.tempStory = imageStories[indexPath.item]
            navigate.tempStoryDp = imageStories[indexPath.item]
            navigate.tempStoryName = imageStories[indexPath.item]
//            navigate.storyDpName.text
            present(navigate, animated: true)
                
        }else{
            let navigate = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "ProfilePage")as!ProfilePage
            navigate.str =  followersDp[indexPath.item]
            navigate.str2 = followesName[indexPath.item]
            self.navigationController?.pushViewController(navigate, animated: true)
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (collectionStories.frame.size.width - 30)/4.5
        let height = collectionStories.frame.size.height
        return CGSize(width: width, height: height)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {                        //hiding using scrolling distance
        guard (scrollView as? UICollectionView) == collectionHomesPosts else {
            print("scroll not working")
            return
        }
        let translation = scrollView.panGestureRecognizer.translation(in: self.view)
        if translation.y < 10 {
            self.heightStory.constant = 0
        }else{
            self.heightStory.constant = 98
        }
    }
   
    
    
    
    
    
    @IBOutlet weak var addPostImage: UIImageView!
    @IBOutlet weak var buttonAddPost: UIButton!
    
    @IBOutlet weak var imageMessenger: UIImageView!
    @IBOutlet weak var buttonMessenger: UIButton!
    @IBOutlet weak var collectionStories: UICollectionView!
    @IBOutlet weak var viewCollectionStories: UIView!
    @IBOutlet weak var collectionHomesPosts: UICollectionView!
    @IBOutlet weak var heightStory: NSLayoutConstraint!
    
    @IBOutlet weak var viewCollectionHomePosts: UIView!
    var temp:Int!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        

        // Do any additional setup after loading the view.
    }
    
    @IBAction func ButtonMessengerAction(_ sender: Any) {
        let story = UIStoryboard(name: "Main", bundle: nil)
        let messenger = story.instantiateViewController(identifier: "MessengerPage")as! MessengerPage
        
        navigationController?.pushViewController(messenger, animated: true)
//        present(messenger, animated: true)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
