//
//  SearchPageImageCell.swift
//  instagram demo
//
//  Created by RP-14 on 11/05/22.
//

import UIKit

class SearchPageImageCell: UICollectionViewCell {
    
    @IBOutlet weak var searchPageImage: UIImageView!
}
