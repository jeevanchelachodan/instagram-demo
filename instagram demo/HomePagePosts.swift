//
//  HomePagePosts.swift
//  instagram demo
//
//  Created by RP-14 on 06/05/22.
//

import UIKit

class HomePagePosts: UICollectionViewCell {
    @IBOutlet weak var homePageFollowersDp: UIImageView!
    
    @IBOutlet weak var homePageFollowersName: UILabel!
    @IBOutlet weak var homePagePostedImages: UIImageView!
    
    @IBOutlet weak var hpLikeView: UIView!
    
    @IBOutlet weak var hpLikeImage: UIImageView!
    
    @IBOutlet weak var hpCommentView: UIView!
    @IBOutlet weak var hpCommentImage: UIImageView!
    @IBOutlet weak var hpShareView: UIView!
    
    @IBOutlet weak var hpShareImage: UIImageView!
    
    @IBOutlet weak var hpSaveView: UIView!
    @IBOutlet weak var hpSaveImage: UIImageView!
    var temp = 0
    @IBAction func likeButtonAction(_ sender: Any) {
        if temp == 0 {
            hpLikeImage.image = UIImage(named: "heart-2")
            temp = 1
        }else{
            hpLikeImage.image = UIImage(named: "heart-3")
            temp = 0
        }
    }
    @IBAction func gotoProfileButton(_ sender: Any) {
    }
}
