//
//  StoryOpenPage.swift
//  instagram demo
//
//  Created by RP-14 on 10/05/22.
//

import UIKit

class StoryOpenPage: UIViewController {
    @IBOutlet weak var storyDp: UIImageView!
    var tempStory = ""
    var tempStoryDp = ""
    var tempStoryName = ""
    @IBOutlet weak var storyDpName: UILabel!
    @IBOutlet weak var storyFullSizeImage: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        storyDp.image = UIImage(named: tempStoryDp)
        storyDpName.text = tempStoryName
        storyFullSizeImage.image = UIImage(named: tempStory)
        storyDp.layer.cornerRadius = 30
        

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
